package com.codility.tasks.hibernate.solution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.logging.Logger;

@Entity
class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    private Long priority;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }
}

@RestController
@RequestMapping("/tasks")
class TaskController {
    private static Logger log = Logger.getLogger("Solution");

    @Autowired
    private TaskRepository taskRepository;

    @Transactional
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Object updateTask(@PathVariable Long id, @RequestBody(required = false) Task requestBody) {
        if (requestBody == null || requestBody.getDescription() == null) {
            return new ErrorResponse(HttpStatus.BAD_REQUEST, "Task description is required");
        }

        Optional<Task> optionalTask = taskRepository.findById(id);
        if (!optionalTask.isPresent()) {
            return new ErrorResponse(HttpStatus.NOT_FOUND, "Cannot find task with given id");
        }

        Task existingTask = optionalTask.get();
        existingTask.setDescription(requestBody.getDescription());
        existingTask.setPriority(requestBody.getPriority());
        taskRepository.save(existingTask);

        return requestBody;
    }

    static class ErrorResponse {
        private final String message;
        private final int status;

        public ErrorResponse(HttpStatus status, String message) {
            this.status = status.value();
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public int getStatus() {
            return status;
        }
    }
}

interface TaskRepository extends JpaRepository<Task, Long> {

}
